import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func

Base = declarative_base()

class CommonMixture:
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)

def add(function):
    def wrapper(self, *args, **kwargs):
        record = function(self, *args, **kwargs)
        self._session.add(record)
        self._session.commit()
        return record
    return wrapper
class User:
    def __init__(self, engine, name, host=None, port=None, username=None, password=None, **params):
        opertor = f"{engine}://"
        if username is not None:
            opertor += username
        if password is not None:
            opertor += f":{password}"
        if username is not None:
            opertor += "@"
        if host is not None:
            opertor += f"{host}"
        if port is not None:
            opertor += f":{port}"
        opertor += f"/{name}"
        if params:opertor += "?"
        opertor += "&".join(f"{key}={value}" for key, value in params.items())
        self._engine = sa.create_engine(opertor)
        Base.metadata.create_all(self._engine)
        self._session = sessionmaker(bind=self._engine)()

class Product(CommonMixture, Base):
    __tablename__ = "Products"
    name = sa.Column(sa.Unicode(64), unique=True, nullable=False)


class Price(CommonMixture, Base):
    __tablename__ = "Prices"
    price = sa.Column(sa.Float, sa.CheckConstraint("price>0"), nullable=False)
    product_id = sa.Column(
        sa.Integer,
        sa.ForeignKey(f"{Product.__tablename__}.id"),
        nullable=False
    )


class Sale(CommonMixture, Base):
    __tablename__ = "Sales"
    product_id = sa.Column(
        sa.Integer,
        sa.ForeignKey(f"{Product.__tablename__}.id"),
        nullable=False
    )
    quantity = sa.Column(sa.Float, nullable=False)



class Buyer(User):
    def product_check(self, name):
        return self._session.query(Product).filter(Product.name == name).first()
    @add
    def income(self, name, quantity,):
        product = self.product_check(name)
        if product is None:
            raise Exception("Cant get any new product")
        return Sale(product_id=product.id, quantity=quantity)



class Seller(User):
    def product_check(self, name):
        return self._session.query(Product).filter(Product.name == name).first()

    def quantity_product(self, product):
        return self._session.query(func.sum(Sale.quantity).label("quantity")).filter(
            Sale.product_id == product.id,
        ).first()[0]

    @add
    def sale(self, name, quantity):
        product = self.product_check(name)
        if product is None:
            raise Exception("No product '%s'" % name)
        quantitystock = self.quantity_product(product)
        if quantity > quantitystock:
            raise Exception("not enough  %f for trade '%s'. quantity: %f" % (
                quantity, name, quantitystock,
            ))
        return Sale(product_id=product.id, quantity=-quantity)

class Sales(Seller, Buyer):
    @add
    def margin(self, name, quantity, price):
        product = Product(name=name)
        self._session.add(product)
        self._session.commit()
        price = Price(price=price, product_id=product.id)
        self._session.add(price)
        self._session.commit()
        return Sale(product_id=product.id, quantity=quantity)



company = Sales(engine="sqlite", name="store.db")

buyer = Buyer(engine="sqlite", name="store.db")
seller = Seller(engine="sqlite", name="store.db")

company.margin(name="столбы", quantity=5, price=35)
company.margin(name="профлист", quantity=10, price=56)
company.margin(name="лопата", quantity=15, price=74)

company.sale(name="лопата", quantity=32)
