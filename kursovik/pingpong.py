from tkinter import *
import random

# Создаем поле, двигаем мяч, управляем ракетками, создаем отскок, считаем очки и респауним мяч

# Глобальные переменные
WIDTH = 1800  # ширина окна
HEIGHT = 600  # высота окна окна
# Очки игроков
PLAYER_1_SCORE = 0
PLAYER_2_SCORE = 0
INITIAL_SPEED = 20  # скорость для респауна
PAD_W = 15  # ширина ракетки
PAD_H = 150  # высота ракетки
BALL_RADIUS = 40  # мяч, радиус
# скорость мяча
BALL_X_CHANGE = 20  # горизонтальная скорость, пусть летит строго вправо, т.к. Y = 0
BALL_Y_CHANGE = 0  # вертикальная скорость,

# Создаем окно
root = Tk()
root.title("Ping-Pong")

# Cоздаем экземпляр класса Canvas
c = Canvas(root, width=WIDTH, height=HEIGHT, background="green")
c.pack()

# Элементы игрового поля:
c.create_line(PAD_W, 0, PAD_W, HEIGHT, fill="black")  # левая линия (линии рисуем c.create_line)
c.create_line(WIDTH - PAD_W, 0, WIDTH - PAD_W, HEIGHT, fill="black")  # правая линия
c.create_line(WIDTH / 2, 0, WIDTH / 2, HEIGHT, fill="white")  # Сетка
BALL = c.create_oval(WIDTH / 2 - BALL_RADIUS / 2,  # мяч рисуем методом c.create_oval
                     HEIGHT / 2 - BALL_RADIUS / 2,
                     WIDTH / 2 + BALL_RADIUS / 2,
                     HEIGHT / 2 + BALL_RADIUS / 2,
                     fill="white")
# Ракетки
LEFT_PAD = c.create_line(PAD_W / 2, 0, PAD_W / 2, PAD_H, width=PAD_W, fill='red')  # Левая ракетка
RIGHT_PAD = c.create_line(WIDTH - PAD_W / 2, 0, WIDTH - PAD_W / 2, PAD_H, width=PAD_W, fill='blue')  # Правая ракетка

# Очки, на поле, текстом
p_1_text = c.create_text(WIDTH - WIDTH / 6, PAD_H / 4, text=PLAYER_1_SCORE, font='Arial 20', fill='black')
p_2_text = c.create_text(WIDTH / 6, PAD_H / 4, text=PLAYER_2_SCORE, font='Arial 20', fill='black')

PAD_SPEED = 20  # Скорости ракеток
RIGHT_PAD_SPEED = 0  # Начальная скорость правой
LEFT_PAD_SPEED = 0  # Начальная скорость левой

# Скорость мяча с каждым ударом
BALL_SPEED_UP = 1.00  # Увеличение скорости мяча
BALL_MAX_SPEED = 30  # Максимальная скорость мяча
BALL_X_SPEED = 20  # Начальная скорость мяча по горизонтали
BALL_Y_SPEED = 20  # Начальная скорость мяча по вертикали
# Расстояние до правого края
right_line_distance = WIDTH - PAD_W


# Функция счета
def score(player):
    global PLAYER_1_SCORE, PLAYER_2_SCORE
    if player == 'right':
        PLAYER_1_SCORE += 1
        c.itemconfig(p_1_text, text=PLAYER_1_SCORE)  # методом itemconfig() меняем текст, прибавляем очко
    else:
        PLAYER_2_SCORE += 1
        c.itemconfig(p_2_text, text=PLAYER_2_SCORE)


# Функция респауна # размещаем мяч по центру
def respawn():
    global BALL_X_SPEED
    c.coords(BALL, WIDTH / 2 - BALL_RADIUS / 2, HEIGHT / 2 - BALL_RADIUS / 2, WIDTH / 2 + BALL_RADIUS / 2,
             HEIGHT / 2 + BALL_RADIUS / 2)
    BALL_X_SPEED = - (BALL_X_SPEED * -INITIAL_SPEED) / abs(BALL_X_SPEED)


# Функция отскока мяча от ракеток
def otskok(action):  # Передаем параметр экшн в функцию
    global BALL_X_SPEED, BALL_Y_SPEED
    if action == 'strike':
        BALL_Y_SPEED = random.randrange(-15, 15)  # Рандомом задаем направление движения мяча
        if abs(BALL_X_SPEED) < BALL_MAX_SPEED:  # Если скорость x мяча меньше максимальной, то
            BALL_X_SPEED *= -BALL_SPEED_UP  # увеличиваем скорость и меняем на отрицательную, т.е. в другую сторону по х
        else:
            BALL_X_SPEED = - BALL_X_SPEED  # если уже максимальная, то просто меняем направление (-) по х
    else:
        BALL_Y_SPEED = - BALL_Y_SPEED  # меняем направление по y


# функция движения мяча o_O
def move_ball():
    ball_left, ball_top, ball_right, ball_bottom = c.coords(
        BALL)  # c.coords` ом определяем положение мяча BALL, он возвращает позицию
    ball_center = (ball_top + ball_bottom) / 2  # высчитываем центер мяча
    # Вертикальный отскок т.к. снизу и сверху очки не считаются, и если мы далеко от вертикальных линий, то двигаем мяч
    if ball_right + BALL_X_SPEED < right_line_distance and ball_left + BALL_X_SPEED > PAD_W:
        c.move(BALL, BALL_X_SPEED, BALL_Y_SPEED)  # Метод move() объекта Canvas двигает мяч дальше
    # Если мы далеко, то двигаем мяч иначе если мячь касается правой или левой стороны, то проверяем какой стороны
    # коснулись если правой то сравинваем позицию центар мыча с позициеей правой ракетки, если мяч в пределах ракетки
    # то делаем отскок иначе мы проигрываем и вызываем score и respawn
    elif ball_right == right_line_distance or ball_left == PAD_W:  # если коснулись левой или правой линии
        if ball_right > WIDTH / 2:  # проверяем левой или правой стороны мы коснулись, т.е.бол райт больше ширина окна/2
            # сравниваем позицию центра мяча с позицией ракетки, если он в пределах делаем отскок
            if c.coords(RIGHT_PAD)[1] < ball_center < c.coords(RIGHT_PAD)[3]:
                # т.е. если правая ракетка < [1] чем бол центер и < чем правая ракетка [3],# то экшен страйк
                otskok('strike')
            else:
                score('left')  # если мяч пропустил т.е. центра мяча не в позиции ракетки, то  обновляем у левого счет
                respawn()  # и респауним мяч
        else:
            if c.coords(LEFT_PAD)[1] < ball_center < c.coords(LEFT_PAD)[3]:  # тоже самое про левую ракетку
                otskok('strike')
            else:
                score('right')  # обновляем у правого счет
                respawn()  # и респауним мяч
    else:  # проверка ситуации что мяч за границей поля, то двигаем мяч дальше
        if ball_right > WIDTH / 2:
            c.move(BALL, right_line_distance - ball_right,
                   BALL_Y_SPEED)  # двигаем к правой линии, уменьшая расстояние, со скоростью по Y
        else:  # иначе если коснулись то отскакиваем
            c.move(BALL, -ball_left + PAD_W, BALL_Y_SPEED)  # минус потому что отскакиваем, отскок со скоростью Y
    if ball_top + BALL_Y_SPEED < 0 or ball_bottom + BALL_Y_SPEED > HEIGHT:  # горизонтальный отскок  вверх или вниз
        otskok("ricochet")


# функция движения ракеток
def move_pads():
    pads = {LEFT_PAD: LEFT_PAD_SPEED,  # присваиваем скорости ракеток ракеткам в словаре, т.к. словарь это ключ и
            RIGHT_PAD: RIGHT_PAD_SPEED}  # значение, просто удобно
    for pad in pads:  # перебираем ракетки циклом и придаем им движение
        c.move(pad, 0, pads[pad])  # pad двигается ракетка со скоростью падс
        if c.coords(pad)[1] < 0:  # если вверх ушла ,   возвращаем c.coords()`ом
            c.move(pad, 0, -c.coords(pad)[1])  # если вверх ушла, возвращаем ее на место
        elif c.coords(pad)[3] > HEIGHT:  # если вниз ушла
            c.move(pad, 0, HEIGHT - c.coords(pad)[3])  # то возвращаем на место


# главная функция в которой вызываем движение мяча и ракетки, с помощью рекурсии
def main():
    move_ball()  # просто вызываем движение мяча
    move_pads()  # и ракеток
    # вызываем саму себя, получается рекурсия, чтоб крутилось постоянно
    root.after(25, main)  # Метод after() вызывает функцию мэйн, через количество миллисекунд 30


# Фокус на канвас, получаем события с клавиатуры
c.focus_set()


# обработка нажатий клавиш
def moveent_handler(event):
    global LEFT_PAD_SPEED, RIGHT_PAD_SPEED
    if event.keysym == 'w':  # если событие нажатие W,
        LEFT_PAD_SPEED = -PAD_SPEED  # то присвоим скорость ... минус вверх
    elif event.keysym == 's':  # если событие, то присвоим скорость  отрицательную
        LEFT_PAD_SPEED = PAD_SPEED
    elif event.keysym == 'Up':  # тоже самое
        RIGHT_PAD_SPEED = -PAD_SPEED
    elif event.keysym == 'Down':  # тоже самое
        RIGHT_PAD_SPEED = PAD_SPEED


# Привязка к канвас биндом bind() между собой связываем событие клавиша нажата и действие
c.bind("<KeyPress>", moveent_handler)


# Функции клавиши не нажаты, т.е. ракетки не двигаются
def stop_pad(event):
    global LEFT_PAD_SPEED, RIGHT_PAD_SPEED
    if event.keysym in ('w', 's', 'W', 'S'):  # если клавиши не нажаты
        LEFT_PAD_SPEED = 0  # скорость равна 0
    elif event.keysym in ('Up', 'Down'):  # тоже самое
        RIGHT_PAD_SPEED = 0


# Привязка к канвас биндом bind() между собой связываем событие клавиша НЕ нажата и действие
c.bind("<KeyRelease>", stop_pad)

# запуск функции мэйн
main()

# запуск окна
root.mainloop()
