from calc import operations  # import ops from calc
# Между импортами и кодом должно быть две пустые строки

operation = input('''
+ for addition
- for subtraction
* for multiplication
/ for division
** for power
% for modulo
''')

x = float(input())
y = float(input())

if operation == "+":
    print(operations.addition(x, y))
elif operation == "-":
    print(operations.subtraction(x, y))
elif operation == "*":
    print(operations.multiplication(x, y))
elif operation == "/":
    print(operations.division(x, y))
elif operation == "**":
    print(operations.power(x, y))
elif operation == "%":
    print(operations.modulo(x, y))
else:
    print("Not correct operation, try again")
