import sys
import socket
from threading import Thread
from PyQt5 import QtWidgets
from gui import Ui_MainWindow

# Create app
app = QtWidgets.QApplication(sys.argv)

# Create form ui
MainWindow = QtWidgets.QMainWindow()
ui = Ui_MainWindow()
ui.setupUi(MainWindow)
MainWindow.show()


# Programm

def srequest():  # Server request
    while socks._closed is False:
        srequest = conn.recv(1024).decode("utf-8")
        print(f"client > '{srequest}'")


def sresponse():  # Server response
    while socks._closed is False:
        sresponse = input(ui.textEdit)
        conn.send(sresponse.encode("utf-8"))
        print(f"server > '{sresponse}'")


def crequest():  # Client request
    while sockc._closed is False:
        crequest = input(ui.textEdit)
        sockc.send(crequest.encode("utf-8"))
        print(f"client > '{crequest}'")


def cresponse():  # Client response
    while sockc._closed is False:
        cresponse = sockc.recv(1024).decode("utf-8")
        print(f"server > '{cresponse}'")


socks = socket.socket()
socks.bind(("", 8000))
socks.listen(1)
conn, addrs = socks.accept()
print(f"{addrs[0]}:{addrs[1]} connected")
sthread1 = Thread(target=srequest)
sthread2 = Thread(target=sresponse)

sockc = socket.socket()
addrc = ("127.0.0.1", 8000)
sockc.connect(addrc)
print(f"{addrc[0]}:{addrc[1]} connected")
cthread1 = Thread(target=crequest)
cthread2 = Thread(target=cresponse)


def pushbutt1():
    if ui.radioButton.isChecked():
        sthread1.start()
        sthread2.start()
        cthread1.start()
        cthread2.start()
    elif ui.radioButton_2.isChecked():
        sthread1.start()
        sthread2.start()
        cthread1.start()
        cthread2.start()
    else:
        pass


def pussbutt2():
    if sresponse() == True:
        ui.listWidget.addItem(ui.textEdit())
    elif srequest() == True:
        ui.listWidget.addItem(ui.textEdit())
    elif cresponse() == True:
        ui.listWidget.addItem(ui.textEdit())
    elif crequest() == True:
        ui.listWidget.addItem(ui.textEdit())
    else:
        pass


ui.pushButton.clicked.connect(pushbutt1)
ui.pushButton_2.clicked.connect(pussbutt2)

# main loop

sys.exit(app.exec_())
